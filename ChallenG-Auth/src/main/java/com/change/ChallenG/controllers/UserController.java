package com.change.ChallenG.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.change.ChallenG.models.ERole;
import com.change.ChallenG.models.Role;
import com.change.ChallenG.models.User;
import com.change.ChallenG.payload.request.AddRequest;
import com.change.ChallenG.payload.request.UpdateRequest;
import com.change.ChallenG.payload.response.MessageResponse;
import com.change.ChallenG.repository.RoleRepository;
import com.change.ChallenG.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	PasswordEncoder encoder;
	
	@GetMapping("/users")
	public List<User> getAllUsers(){
		PageRequest page = PageRequest.of(
				0, 6, Sort.by("username").ascending());
		List<User> users = userRepository.findAll(page).getContent();
		return users;
	}
	
	@GetMapping("/users/{username}")
	public List<User> getUser(@PathVariable String username){
		List<User> users = userRepository.getByUsernameContainingOrEmailContaining(username,username);
		return users;
		//use Optional and ResponseEntity
		//return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/users/user/{username}")
	public User getOneUser(@PathVariable String username){
		User user = userRepository.findByUsername(username).
				orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
		return user;
	}
	
	@DeleteMapping("/users/{username}")
	public ResponseEntity<?> deleteUser(@PathVariable String username) {
		//change it to id==1
		if(username.contentEquals("admin")) {
			//log.error("Can't delete the administrator!");
			return ResponseEntity.ok(new MessageResponse("Can't delete the administrator!"));}
		else {
			User user=userRepository.findByUsername(username).
					orElseThrow(() -> new EmptyResultDataAccessException(1));
			userRepository.delete(user);	
			//log.info("User deleted successfully!");
			return ResponseEntity.ok(new MessageResponse("User deleted successfully!"));
		}
		
	}
	
	@PutMapping("/users")
	public ResponseEntity<?> updateUser(@RequestBody UpdateRequest updateRequest) {
		
		User user = userRepository.findById(updateRequest.getId()).
				orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " +updateRequest.getUsername()));
		
		if (user != null) {
			Set<Role> roles = new HashSet<>();
			if(updateRequest.getRoles().contains("ROLE_ADMIN")) { 
				roles.add(roleRepository.findByName(ERole.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found.")));
			}
			if(updateRequest.getRoles().contains("ROLE_MODERATOR")) { 
				roles.add(roleRepository.findByName(ERole.ROLE_MODERATOR)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found.")));
			}
			user.setRoles(roles);
			user.setEmail(updateRequest.getEmail());
			user.setUsername(updateRequest.getUsername());
			user.setPassword(encoder.encode(updateRequest.getPassword()));	
			userRepository.save(user);
			//log.info("User modified successfully!");
			return ResponseEntity.ok(new MessageResponse("User modified successfully!"));
		}
		else return ResponseEntity.badRequest().body(new MessageResponse("Enable to update user"));
		
	}
	
	@PostMapping("/users/new")
	public ResponseEntity<?> addUser(@RequestBody AddRequest addRequeest) {

		if (userRepository.existsByUsername(addRequeest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}
		if (userRepository.existsByEmail(addRequeest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}
		User user = new User(addRequeest.getUsername(), addRequeest.getEmail(),
				encoder.encode(addRequeest.getPassword()));

		Set<Role> roles = new HashSet<>();
		if (addRequeest.getRoles().contains("ROLE_ADMIN")) {
			roles.add(roleRepository.findByName(ERole.ROLE_ADMIN)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found.")));
		} else if (addRequeest.getRoles().contains("ROLE_MODERATOR")) {
			roles.add(roleRepository.findByName(ERole.ROLE_MODERATOR)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found.")));
		} else
			roles.add(roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found.")));
		user.setRoles(roles);
		userRepository.save(user);
		//log.info("User successfully created!");
		return ResponseEntity.ok(new MessageResponse("User successfully created!"));

	}
	
}
