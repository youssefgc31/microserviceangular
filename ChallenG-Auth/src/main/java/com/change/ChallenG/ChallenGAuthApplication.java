package com.change.ChallenG;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;

import com.change.ChallenG.models.ERole;
import com.change.ChallenG.models.Role;
import com.change.ChallenG.models.User;
import com.change.ChallenG.repository.RoleRepository;
import com.change.ChallenG.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
public class ChallenGAuthApplication{

	public static void main(String[] args) {
		SpringApplication.run(ChallenGAuthApplication.class, args);
	}

//    @Profile("!prod")
//    @Transactional
//    @Bean
//    CommandLineRunner start(UserRepository userRepository, RoleRepository roleRepository) {
//
//        Role roleAdmin = new Role(ERole.ROLE_ADMIN);
//        Role roleModerator = new Role(ERole.ROLE_MODERATOR);
//        Role roleUser = new Role(ERole.ROLE_USER);
//
//        Stream<Role> streamRoles = Stream.of(roleAdmin, roleModerator, roleUser);
//
//        User admin = new User("youssef", "ygahouchi@gmail.com", "$2a$10$nKnQg/2A6uAROdozdQ7Fr.G1/9OTLTQgntJqzrvZMAD9lIrx7e13K");
//        Set<Role> rolesAdmin = new HashSet<Role>();
//        rolesAdmin.add(roleAdmin);
//        admin.setRoles(rolesAdmin);
//
//
//        if (userRepository.count() <= 0) {
//
//            //log.info("POPULATING DATABASE");
//            streamRoles.forEach(r -> {
//                roleRepository.save(r);
//            });
//            return args -> {
//                Stream.of(admin).forEach(user -> {
//                    userRepository.save(admin);
//                });
//            };
//        }
//        else {
//            //log.info("DATABASE ALREADY INITIALIZED");
//            return args -> {
//                streamRoles.forEach(System.out::print);
//            };
//        }
//    }

}