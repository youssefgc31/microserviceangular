package com.change.ChallenG.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.change.ChallenG.models.User;

@CrossOrigin(origins = "*", maxAge = 3600)
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	Optional<User> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);	
	
	void deleteUserByUsername(String username);
	
	List<User> getByUsernameContainingOrEmailContaining(String username, String email);
}
