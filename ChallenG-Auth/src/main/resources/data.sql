delete from roles;
delete from users;
delete from user_roles;

INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');

INSERT INTO users (id, email, password, username) VALUES (1, 'admin@admin.com', '$2a$10$nKnQg/2A6uAROdozdQ7Fr.G1/9OTLTQgntJqzrvZMAD9lIrx7e13K', 'admin');

INSERT INTO user_roles (user_id, role_id) VALUES (1, 3);

