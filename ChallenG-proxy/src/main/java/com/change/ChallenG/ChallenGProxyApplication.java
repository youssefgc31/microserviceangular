package com.change.ChallenG;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ChallenGProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallenGProxyApplication.class, args);
	}

}
