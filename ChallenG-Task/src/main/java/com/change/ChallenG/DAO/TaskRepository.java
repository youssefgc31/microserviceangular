package com.change.ChallenG.DAO;

import java.util.ArrayList;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.change.ChallenG.Entity.Task;



@RepositoryRestResource
@CrossOrigin("*")
public interface TaskRepository extends JpaRepository<Task, Long> {
	ArrayList<Task> findByUserId(Long userId);

}
