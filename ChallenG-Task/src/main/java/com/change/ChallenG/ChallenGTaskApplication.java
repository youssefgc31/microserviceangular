package com.change.ChallenG;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.change.ChallenG.DAO.TaskRepository;
import com.change.ChallenG.Entity.Task;


@EnableDiscoveryClient
@SpringBootApplication
public class ChallenGTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallenGTaskApplication.class, args);
	}
	
	@Bean
	//@Profile("Default")
	CommandLineRunner start(TaskRepository taskRepository) {
		return args->{
			Stream.of("rabbitMQ","add weebFlux","make it beautiful").forEach(task->{
				taskRepository.save(new Task(null, task , "COMPLETED", LocalDateTime.of(2020,1,1,1,1) ,LocalDateTime.of(1999,1,9,1,1),1));
			});
		};
		
	}
}
