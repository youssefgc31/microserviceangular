package com.change.ChallenG.Security;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration(exclude = { UserDetailsServiceAutoConfiguration.class }) //Disable Spring Security generated password 
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable()
			.authorizeRequests()
			.antMatchers("/**").permitAll()
			.antMatchers("192.168.1.7:4200/**").permitAll()
				.antMatchers("192.168.1.3/**").permitAll()
				.antMatchers("41.142.158.102/**").permitAll()
				.anyRequest().authenticated();;
	}
}
