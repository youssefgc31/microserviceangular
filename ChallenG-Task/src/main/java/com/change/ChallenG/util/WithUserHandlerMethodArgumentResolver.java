package com.change.ChallenG.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.change.ChallenG.Entity.UserInfo;
import com.change.ChallenG.Entity.WithUser;

@Configuration
public class WithUserHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

	
	@Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(WithUser.class) &&
                parameter.getParameterType().equals(UserInfo.class);
    }
	
	//THIS METHOD IS USED TO RESOLVE ARGUMENT WITH A REQUEST
	//WE CAN'T INJECT A @COMPONENT IN A CLASS THAT DOESN'T HAVE A CONSTRUCTOR, ALTHOUGH WE CAN USE @BEAN IF OUR CLASS IS NOT A @COMPONENT,
	//YET IN THIS CLASS TO AVOID ANY CONFLICT WE SEPARATED LOGIC IN "JwtUtils" TO GET THE CONFIGURATION PROPERTY
    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
    	
        HttpServletRequest httpServletRequest = (HttpServletRequest) webRequest.getNativeRequest();
		String username=(String)JwtUtils.getUsernameFromRequest(httpServletRequest).get();
        return new UserInfo(username);
    }

}
