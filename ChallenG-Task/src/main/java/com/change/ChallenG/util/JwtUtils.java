package com.change.ChallenG.util;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.change.ChallenG.config.TasksProperties;

import io.jsonwebtoken.Jwts;

@Component
public class JwtUtils {
	
	
	public static TasksProperties tasksproperties;

//USED CONSTRUCTOR INJECCTION, WORKS BETTER	
	@SuppressWarnings("static-access")
	@Autowired
	public JwtUtils(TasksProperties tasksproperties) {
		this.tasksproperties= tasksproperties;
	}

//GETS THE CURRENT USERNAME USING THE REQUEST'S HEADER JWT AUTORIZATION
	public static Optional<String> getUsernameFromRequest(HttpServletRequest httpServletRequest) {
		String authorizationHeader = httpServletRequest.getHeader("Authorization");
	    String token = authorizationHeader.replace("Bearer ", "");
	    String username = Jwts.parser()
	    		.setSigningKey(tasksproperties.getJwtSecret())
	    		.parseClaimsJws(token)
	    		.getBody()
	    		.getSubject();	
	    Optional<String> optional=Optional.of(username);
	    return optional;
	}

}
