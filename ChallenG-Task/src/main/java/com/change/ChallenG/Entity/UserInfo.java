package com.change.ChallenG.Entity;

public class UserInfo {
	 private String username;

	public UserInfo(String username) {
		super();
		this.username = username;
	}

	public UserInfo() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
