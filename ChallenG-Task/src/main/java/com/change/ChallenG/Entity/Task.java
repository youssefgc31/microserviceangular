/**
 * 
 */
/**
 * @author youssef
 *
 */
package com.change.ChallenG.Entity;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;






@Entity
public class Task {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String label;
	private String state;
	private LocalDateTime dueDate;
	private LocalDateTime creationDate;
//	@ManyToOne
//	private User user;
	private long userId;

	
	public Task() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Task(Long id, String label, String state, LocalDateTime dueDate, LocalDateTime creationDate, long userId) {
		super();
		this.id = id;
		this.label = label;
		this.state = state;
		this.dueDate = dueDate;
		this.creationDate = creationDate;
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "Task [id=" + id + ", label=" + label + ", state=" + state + ", dueDate=" + dueDate
				+ ", creationDate=" + creationDate + "]";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public LocalDateTime getDueDate() {
		return dueDate;
	}
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	public void setDueDate(LocalDateTime dueDate) {
		this.dueDate = dueDate;
	}
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	public LocalDateTime getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
