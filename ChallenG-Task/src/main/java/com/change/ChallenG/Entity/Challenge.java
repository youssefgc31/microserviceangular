package com.change.ChallenG.Entity;


import java.time.Duration;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Challenge{
	private Long id;
    private Duration duration;    
}
