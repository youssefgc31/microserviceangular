package com.change.ChallenG.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.change.ChallenG.DAO.TaskRepository;
import com.change.ChallenG.DAO.UserRepository;
import com.change.ChallenG.Entity.Task;
import com.change.ChallenG.Entity.User;
import com.change.ChallenG.Entity.UserInfo;
import com.change.ChallenG.Entity.WithUser;

import lombok.extern.slf4j.Slf4j;



@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class TaskController {
	
	@Autowired
	TaskRepository taskRepository;
	
	@Autowired
	UserRepository userRepository;
	
	//THIS METHOD USES THE CURRENT USER INJECTED AS A PARAMETER TO GET HIS TASKS
	@GetMapping("/mytasks")
	public List<Task> loadTasksByUser(@WithUser UserInfo userInfo) throws RuntimeException {
		
		//log.info("Loading tasks of the current user");
		try {
			String username=userInfo.getUsername();
			if(!(username==null)) {
				Long userId=loadUserByUsername(username);
				List<Task> tasks = taskRepository.findByUserId(userId);
				return tasks;	
			}
		} catch (NullPointerException e) {
			//log.error("No task to be shown");
		}
		return null;
		
	}
	@Transactional
	public Long loadUserByUsername(String username) throws UsernameNotFoundException{
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
		return user.getId();
	}
	
	@GetMapping("/myusername")
	public String currentUserName(@WithUser UserInfo userInfo) {
		 String username = userInfo.getUsername();
		 return username;
	}
}
