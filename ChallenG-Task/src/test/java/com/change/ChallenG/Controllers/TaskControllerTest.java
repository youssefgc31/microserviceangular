package com.change.ChallenG.Controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.change.ChallenG.controllers.TaskController;

@ExtendWith(SpringExtension.class) 
@WebMvcTest(TaskController.class)
public class TaskControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testMytasksPage() throws Exception{
		//Expects HTTP 200
		mockMvc.perform(get("/myTasks"))
				.andExpect(status().isOk());
		
	}
	

}
