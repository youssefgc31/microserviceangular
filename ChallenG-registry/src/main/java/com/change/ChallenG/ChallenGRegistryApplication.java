package com.change.ChallenG;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
@EnableEurekaServer
@SpringBootApplication
public class ChallenGRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallenGRegistryApplication.class, args);
	}

}
