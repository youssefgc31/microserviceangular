import {Component, OnInit, Optional, ViewEncapsulation} from '@angular/core';
import { UserService } from '../_services/user.service';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {TokenStorageService} from '../_services/token-storage.service';
import {Role, User} from '../_models/user';
import {UsercrudService} from '../_services/usercrud.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../_services/auth.service';
import {first} from 'rxjs/operators';
import { faTrash , faUserShield, faUserGraduate} from '@fortawesome/free-solid-svg-icons';

const API_URL = 'http://localhost:3134/users';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {
  content = '';
  listUsers;
  currentUser = this.token.getUser();
  roles: Array<string> = this.currentUser.roles;
  isAdmin = this.roles.includes('ROLE_ADMIN');
  addForm: FormGroup;
  searchForm: FormGroup;
  formDataEdit: any;
  isEditorChecked: boolean;
  isAdminChecked: boolean;
  faTrash = faTrash;
  faUserShield = faUserShield;
  faUserGraduate = faUserGraduate;


  constructor(private userService: UserService,
              private crudService: UsercrudService,
              private http: HttpClient,
              private token: TokenStorageService,
              private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService) {

  }

  ngOnInit(): void {
    if (this.currentUser){this.http.get(API_URL)
        .subscribe(users => {
          this.listUsers = users;
        },
        err => {
          this.content = JSON.parse(err.error).message;
        }
    );
    }
    this.addForm = this.formBuilder.group({
          id: [''],
          username: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(6)] ],
          isAdmin: [''],
          isEditor: [''],
          roles: ['']
      });

    this.searchForm = this.formBuilder.group({
        username: ['', Validators.required]
    });
  }

  deleteUser(user: User): void {
      alert('sure?');
      this.crudService.deleteUser(user)
        .subscribe( data => {
          this.listUsers = this.listUsers.filter(u => u !== user);
        });
  }

  loadFormToEdit(user: User): void{
        this.crudService.getOneUser(user).pipe(first())
            .subscribe( data => {
                this.formDataEdit = data;
                this.addForm.setValue({
                        id : this.formDataEdit.id,
                        username : this.formDataEdit.username,
                        email : this.formDataEdit.email,
                        password : '',
                        isAdmin: this.isAdministrator( this.formDataEdit),
                        isEditor: this.isEditor( this.formDataEdit),
                        roles: ''
                }
                );
            });
  }

  getUserRolesFromData( data: any): Array<string>{
      const listRoles: Array<string> = Array(0);
      if (data.roles[0] !== undefined)
      {listRoles.push(data.roles[0].name); }
      if (data.roles[1] !== undefined)
      {listRoles.push(data.roles[1].name); }
      if (data.roles[2] !== undefined)
      {listRoles.push(data.roles[2].name); }
      return listRoles;
      }

    isEditor( data: any): boolean{
        if (this.getUserRolesFromData(data).includes('ROLE_MODERATOR')) {return true; }
        else { return false; }
    }

    isAdministrator( data: any): boolean{
        if (this.getUserRolesFromData(data).includes('ROLE_ADMIN')) {return true; }
        else { return false; }
    }

  editUser(): void {
      const roles: Array<string> = Array(0);
      if (this.addForm.value.isAdmin) {roles.push('ROLE_ADMIN'); }
      if (this.addForm.value.isEditor) {roles.push('ROLE_MODERATOR'); }
      this.crudService.updateUser(this.addForm.value, roles).pipe(first())
        .subscribe(
            data => {
                this.router.navigate(['admin']).then(r => this.ngOnInit());
                if (data.status === 200) {
                    alert('User updated successfully.');
                }else {
                    alert(data.message);
                }
            },
            error => {
                alert(error);
            });
  }

    addUser(): void {
      const roles: Array<string> = Array(0);
      if (this.isAdminChecked) {roles.push('ROLE_ADMIN'); }
      if (this.isEditorChecked) {roles.push('ROLE_MODERATOR'); }
      this.crudService.createUser(this.addForm.value, roles).pipe(first())
        .subscribe(
            data => {
                this.router.navigate(['admin']).then(r => this.ngOnInit());
                if (data.status === 200) {
                    alert('User created successfully.');
                }else {
                    alert(data.message);
                }
            },
            error => {
                alert(error);
            });
  }

   findUser(): void {
       this.crudService.getUser(this.searchForm.value)
           .subscribe(data => {
               this.listUsers = data;
           },
           err => {
               this.content = JSON.parse(err.error).message;
           }
       );
   }

}
