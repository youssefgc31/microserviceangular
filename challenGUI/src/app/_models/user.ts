export class Role {
    id: number;
    name: string;
}

export class User{
    id: number;
    username: string;
    email: string;
    password: string;
    roles: Array<string>;
}
