import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { TokenStorageService } from '../_services/token-storage.service';

const API_URL = 'http://localhost:3135';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class TasksComponent implements OnInit {

  currentUser: any;
  listTasks;

  constructor(private httpClient: HttpClient, private token: TokenStorageService) {
  }


  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    if (this.currentUser){
    this.httpClient.get(API_URL + '/mytasks').subscribe(data => {
      this.listTasks = data;
      });
    }
  }


}

