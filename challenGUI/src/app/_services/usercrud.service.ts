import { Injectable } from '@angular/core';
import {User} from '../_models/user';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

const API_URL = 'http://localhost:3134/users/';
@Injectable({
  providedIn: 'root'
})
export class UsercrudService {


  constructor(private http: HttpClient) { }

  getUsers(): Observable <any> {
    return this.http.get(API_URL);
  }

  getUser(user: User): Observable <any> {
    return this.http.get(API_URL + user.username);
  }

  getOneUser(user: User): Observable <any> {
    return this.http.get(API_URL + 'user/' + user.username);
  }

  createUser(user: User, roles: Array<string>): Observable <any>{
    user.roles = roles;
    return this.http.post(API_URL + 'new', user);
  }

  updateUser(user: User, roles: Array<string>): Observable <any>{
    user.roles = roles;
    return this.http.put(API_URL , user);
  }

  deleteUser(user: User): Observable <any>{
    return this.http.delete(API_URL + user.username);
  }
}
